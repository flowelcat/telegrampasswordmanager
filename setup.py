import os
from setuptools import setup

setup(
    name='TelegramPasswordManager',
    version='0.1',
    description='Bot for managing password',
    url='https://gitlab.com/flowelcat/telegram_password_manager',
    author='Flowelcat',
    author_email='flowelcat@gmail.com',
    license='GNU',
    packages=["src", "migrations"],
    zip_safe=False,
    python_requires='>3.6.0',
    entry_points=dict(console_scripts=[
        'startbot = src.app:main',
        'init_db = src.scripts.init_db:main',
        'update_db = src.scripts.update_db:main',
    ]),
    )


if not os.path.exists('logs'):
    os.mkdir('logs')
