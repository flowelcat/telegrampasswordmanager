import logging
from functools import wraps

from src.models import DBSession, User

logger = logging.getLogger(__name__)

def need_user(func):
    @wraps(func)
    def wrapped(self, bot, update, *args, **kwargs):
        if update.message:
            chat_id = update.message.chat_id
        elif update.callback_query:
            chat_id = update.callback_query.message.chat_id
        else:
            return func(self, bot, update, *args, **kwargs)

        if 'user_data' in kwargs.keys():
            if 'restarted' in kwargs['user_data'] and kwargs['user_data']['restarted']:
                kwargs['user_data']['user'] = DBSession.query(User).filter(User.chat_id == chat_id).first()
                kwargs['user_data']['restarted'] = False
            if 'user' not in kwargs['user_data']:
                kwargs['user_data']['user'] = DBSession.query(User).filter(User.chat_id == chat_id).first()
            self.user_data = kwargs['user_data']
        self.bot = bot

        return func(self, bot, update, *args, **kwargs)

    return wrapped


def singleton(cls, *args, **kw):
    instances = {}

    def _singleton():
        if cls not in instances:
            instances[cls] = cls(*args, **kw)
        return instances[cls]

    return _singleton
