import ast
import os
from distutils.util import strtobool


def parse_environ_settings(vars):
    for key in vars:
        if key in os.environ:
            if os.environ[key] == "None":
                vars[key] = None
                continue

            if os.environ[key].startswith('"') and os.environ[key].endswith('"'):
                vars[key] = os.environ[key].replace('"', "")
                continue

            if os.environ[key].startswith('[') and os.environ[key].endswith(']'):
                vars[key] = ast.literal_eval(os.environ[key])
                continue

            if '.' in os.environ[key]:
                try:
                    vars[key] = float(os.environ[key])
                    continue
                except ValueError:
                    pass

            try:
                vars[key] = int(os.environ[key])
                continue
            except ValueError:
                pass

            try:
                vars[key] = bool(strtobool(os.environ[key]))
                continue
            except ValueError:
                pass

                vars[key] = os.environ[key]
