import gettext
import os
from os import path

from src.models import Languages
from src.settings import PROJECT_ROOT

langs = {Languages.en: gettext,
         Languages.ru: gettext.translation('base', path.join(PROJECT_ROOT, '..', 'locales'), languages=['ru'])
         }


def generate_underscore(user):
    if user.lang is None:
        return gettext.gettext
    else:
        return langs[user.lang].gettext


def get_langs():
    locales_path = path.join(PROJECT_ROOT, '..', 'locales')
    langs = []
    for item in os.listdir(locales_path):
        if os.path.isdir(path.join(locales_path, item)):
            t = gettext.translation('base', path.join(PROJECT_ROOT, '..', 'locales'), languages=[item])
            langs.append(t.gettext)
    return langs
