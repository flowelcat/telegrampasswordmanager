import logging

import coloredlogs
from sqlalchemy.exc import SQLAlchemyError
from psycopg2 import IntegrityError

from src.models import *

logger = logging.getLogger(__name__)
coloredlogs.install()


def get_class_by_tablename(tablename):
    """Return class reference mapped to table.

  :param tablename: String with name of table.
  :return: Class reference or None.
  """
    for c in Base._decl_class_registry.values():
        if hasattr(c, '__tablename__') and c.__tablename__ == tablename:
            return c


def main():
    """
    Sets default values to old entries in db
    :return:
    """
    updated_count = 0
    for t in Base.metadata.sorted_tables:
        for c in [col for col in t._columns if col.default]:
            model = get_class_by_tablename(t.name)
            default_value = c.default.arg
            default_field_name = c.name
            objs = DBSession.query(model).filter(getattr(model, default_field_name) == None)
            for obj in objs:
                setattr(obj, default_field_name, default_value)
                logger.debug(f"Updating model {model} field {default_field_name} with value {default_value}")
                DBSession.add(obj)
                updated_count += 1
    logger.info(f"Finished, updated {updated_count} entries")

    try:
        DBSession.commit()
    except (SQLAlchemyError, IntegrityError) as e:
        logger.critical("Database error")
        logger.debug(f"Error message {str(e)}")
        DBSession.rollback()


if __name__ == "__main__":
    main()
