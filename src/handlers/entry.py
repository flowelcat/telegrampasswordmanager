import enum
import logging

from emoji import emojize
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import ConversationHandler, MessageHandler, filters, CallbackQueryHandler, CommandHandler

from src.handlers.add_edit import EditMenu
from src.handlers.base import BaseMenu
from src.models import DBSession, DatabaseEntry

logger = logging.getLogger(__name__)


class EntryStates(enum.Enum):
    ACTION = 1
    DELETE_CONFIRMATION = 2


class EntryMenu(BaseMenu):

    def entry(self, bot, update, user_data):
        _ = user_data['_']
        user = user_data['user']

        data = int(update.callback_query.data.replace("entry_", ""))

        entry = user_data['database_menu']['tpd'].get_entry(data)

        user_data['entry_menu'] = {}
        user_data['entry_menu']['entry'] = entry

        self.send_message(user_data)

        bot.answer_callback_query(update.callback_query.id)

        return EntryStates.ACTION

    def send_message(self, user_data):
        _ = user_data['_']
        user = user_data['user']

        entry = user_data['entry_menu']['entry']
        message_text = f"_______{entry.title}_______\n"
        message_text += f"Title:{entry.title}\n"
        message_text += f"Username:{entry.username}\n"
        message_text += f"Password:{entry.password}\n"
        message_text += f"URL:{entry.url}\n"
        message_text += f"Notes:{entry.notes}\n"

        buttons = [[InlineKeyboardButton(emojize(":memo:"), callback_data=f"edit_entry_{entry.id}"),
                    InlineKeyboardButton(emojize(":up_arrow:"), callback_data="back"),
                    InlineKeyboardButton(emojize(":repeat_button:"), callback_data="resend"),
                    InlineKeyboardButton(emojize(":cross_mark:"), callback_data="delete")]]

        if 'interface' in user_data and user_data['interface'] is not None:
            user_data['entry_menu']['interface'] = user_data['interface'].edit_text(text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        else:
            user_data['interface'] = self.bot.send_message(chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
            user_data['entry_menu']['interface'] = user_data['interface']

    def delete(self, bot, update, user_data):
        _ = user_data['_']
        user = user_data['user']

        reply_markup = InlineKeyboardMarkup([[InlineKeyboardButton(emojize(":white_heavy_check_mark: ") + _("Yes") + emojize(" :white_heavy_check_mark:"), callback_data='delete_yes')],
                                             [InlineKeyboardButton(emojize(":no_entry:️ ") + _("No") + emojize(" :no_entry:️"), callback_data='delete_no')]])

        user_data['interface'] = user_data['interface'].edit_text(text=_("You really want to delete {name} entry?").format(name=user_data['entry_menu']['entry'].title), reply_markup=reply_markup)

        bot.answer_callback_query(update.callback_query.id)
        return EntryStates.DELETE_CONFIRMATION

    def delete_confirmation(self, bot, update, user_data):
        _ = user_data['_']
        user = user_data['user']

        data = update.callback_query.data
        if data == 'delete_yes':
            entry = DBSession.query(DatabaseEntry).filter(DatabaseEntry.id == user_data['entry_menu']['entry'].id).first()

            if not self.remove_from_db(user_data, entry):
                return self.conv_fallback(user_data)
            user_data['database_menu']['tpd'].delete_entry(user_data['entry_menu']['entry'])

            if user_data['entry_menu']['entry'].group_id is not None:
                user_data['active_group'] = user_data['database_menu']['tpd'].get_group(user_data['entry_menu']['entry'].group_id)
            else:
                user_data['active_group'] = None

            user_data['entry_menu']['entry'] = None

            user_data['interface'].edit_text(text="Entry deleted")
            user_data['interface'] = None
            self.send_message_view(user_data)
            bot.answer_callback_query(update.callback_query.id)

            return ConversationHandler.END

        elif data == 'delete_no':
            self.send_message(user_data)
            bot.answer_callback_query(update.callback_query.id)

            return EntryStates.ACTION

    def back(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        if 'active_group' in user_data and not hasattr(user_data['active_group'], 'fake'):
            if user_data['entry_menu']['entry'].group_id is not None:
                user_data['active_group'] = user_data['database_menu']['tpd'].get_group(user_data['entry_menu']['entry'].group_id)
            else:
                user_data['active_group'] = None

        self.send_message_view(user_data)

        bot.answer_callback_query(update.callback_query.id)
        return ConversationHandler.END

    def import_error(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        user_data['interface'].delete()

        user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("For importing you mast be in root group."))
        user_data['interface'] = None

        self.send_message(user_data)

        return EntryStates.ACTION

    def search(self, bot, update, user_data, update_queue):
        update_queue.put(update)
        return ConversationHandler.END

    def get_handler(self):
        edit_menu = EditMenu()
        self.menus.append(edit_menu)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^entry_\d+', pass_user_data=True)],
                                      states={
                                          EntryStates.ACTION: [edit_menu.handler,
                                                               CommandHandler('import', self.import_error, pass_user_data=True),
                                                               CommandHandler('search', self.search, pass_user_data=True, pass_update_queue=True),
                                                               CallbackQueryHandler(self.back, pattern=r'^back$', pass_user_data=True),
                                                               CallbackQueryHandler(self.delete, pattern=r'^delete$', pass_user_data=True)],

                                          EntryStates.DELETE_CONFIRMATION: [CallbackQueryHandler(self.delete_confirmation, pattern=r'delete_(yes|no)$', pass_user_data=True)]
                                      },
                                      fallbacks=[MessageHandler(filters.Filters.all, self.unknown_command(-1), pass_user_data=True)],
                                      allow_reentry=True)

        return handler
