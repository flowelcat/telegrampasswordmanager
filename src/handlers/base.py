import gettext
import logging
import os
import pickle
from collections import defaultdict
from math import ceil

from emoji import emojize
from sqlalchemy.exc import SQLAlchemyError, IntegrityError
from telegram import Bot, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.error import BadRequest, TelegramError
from telegram.ext import ConversationHandler, CallbackQueryHandler
from telegram.utils.promise import Promise

from src.lib.translations import generate_underscore
from src.models import DBSession, TelegramPasswordDatabase, TelegramPasswordGroup, User
from src.settings import RESOURCES_FOLDER, BOT_TOKEN, PROJECT_ROOT, ITEMS_PER_PAGE

logger = logging.getLogger(__name__)

STATES_PICKLE = os.path.join(RESOURCES_FOLDER, 'states')
USERDATA_PICKLE = os.path.join(RESOURCES_FOLDER, 'userdata')


class BaseMenu:
    bot = Bot(token=BOT_TOKEN)

    def __init__(self):
        self.menus = []
        self.handler = self.get_handler()

    def databases_markup(self, user_data):
        user = user_data['user']
        _ = user_data['_']
        return InlineKeyboardMarkup([[InlineKeyboardButton(db.name, callback_data=f"database_{db.id}")] for db in user.password_databases] + ([[InlineKeyboardButton(_('---Create new---'), callback_data='new_database')]] if len(user.password_databases) < 5 else []))

    def send_message_view(self, user_data):
        _ = user_data['_']
        user = user_data['user']
        items_count = 0
        is_fake = False

        if 'group_menu' not in user_data:
            user_data['group_menu'] = {}

        if 'active_group' in user_data and user_data['active_group']:
            group = user_data['active_group']
            menu_name = 'group_menu'
            if hasattr(group, 'fake'):
                is_fake = group.fake
        else:
            group = user_data['database_menu']['tpd']
            menu_name = 'database_menu'

        buttons = [[InlineKeyboardButton(emojize(":reverse_button:"), callback_data="previous_page"),
                    InlineKeyboardButton(emojize(":up_arrow:"), callback_data="exit") if isinstance(group, TelegramPasswordDatabase) else InlineKeyboardButton(emojize(":up_arrow:"), callback_data="back"),
                    InlineKeyboardButton(emojize(":NEW_button:"), callback_data="create_new") if not is_fake else InlineKeyboardButton(emojize(":repeat_button:"), callback_data="resend"),
                    InlineKeyboardButton(emojize(":play_button:"), callback_data="next_page")]]

        if not is_fake:
            buttons.append([(InlineKeyboardButton(emojize(":memo:"), callback_data="edit_database") if isinstance(group, TelegramPasswordDatabase) else InlineKeyboardButton(emojize(":memo:"), callback_data=f"edit_group_{group.id}")),
                            InlineKeyboardButton(emojize(":repeat_button:"), callback_data="resend"),
                            InlineKeyboardButton(emojize(":down_arrow:"), callback_data="download"),
                            (InlineKeyboardButton(emojize(":cross_mark:"), callback_data="delete_database") if isinstance(group, TelegramPasswordDatabase) else InlineKeyboardButton(emojize(":cross_mark:"), callback_data="delete"))])

        max_page = ceil((len(group.entries) + len(group.groups)) / ITEMS_PER_PAGE)
        if max_page == 0:
            max_page = 1

        if 'page' not in user_data[menu_name]:
            user_data[menu_name]['page'] = 0
        else:
            if user_data[menu_name]['page'] < 0:
                user_data[menu_name]['page'] = max_page - 1
            elif user_data[menu_name]['page'] + 1 > max_page:
                user_data[menu_name]['page'] = 0

        message_text = f"_______{group.title}_______"
        if group.groups or group.entries:
            for item in (group.groups + group.entries)[ITEMS_PER_PAGE * user_data[menu_name]['page']:]:
                message_text += emojize(f"\n{':file_folder:' if isinstance(item, TelegramPasswordGroup) else ':key:'}  {item.title}")
                buttons.append([InlineKeyboardButton(item.title, callback_data=f"{'group' if isinstance(item, TelegramPasswordGroup) else 'entry'}_{item.id}")])
                items_count += 1
                if items_count >= ITEMS_PER_PAGE:
                    break
        else:
            message_text += '\n' + _('There is no groups or entries yet.')
        message_text += '\n' + _("_______Page {page} of {pages}_______").format(page=user_data[menu_name]['page'] + 1, pages=max_page)

        try:
            assert 'interface' in user_data and user_data['interface'] is not None
            user_data['interface'] = user_data['interface'].edit_text(text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
            user_data[menu_name]['interface'] = user_data['interface']
            user_data[menu_name]['markup'] = InlineKeyboardMarkup(buttons)
        except (TelegramError, AssertionError):
            user_data['interface'] = self.bot.send_message(chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
            user_data[menu_name]['interface'] = user_data['interface']
            user_data[menu_name]['markup'] = InlineKeyboardMarkup(buttons)

    def resend(self, state):
        def resend_calback(bot, update, user_data):
            user_data['interface'].delete()
            user_data['interface'] = None
            self.send_message_view(user_data)
            return state

        handler = CallbackQueryHandler(resend_calback, pattern='^resend$', pass_user_data=True)
        return handler

    def to_state(self, state):
        def to_state_callback(bot, update):
            return state

        return to_state_callback

    def back_markup(self, user_data):
        _ = user_data['_']
        return InlineKeyboardMarkup([[InlineKeyboardButton(_('Back'), callback_data='back')]], resize_keyboard=True)

    def save_states(self, dispatcher):
        # Before pickling
        resolved = dict()
        for k, v in self.handler.conversations.items():
            if isinstance(v, tuple) and len(v) is 2 and isinstance(v[1], Promise):
                try:
                    new_state = v[1].result()  # Result of async function
                except:
                    new_state = v[0]  # In case async function raised an error, fallback to old state
                resolved[k] = new_state
            else:
                resolved[k] = v

        # creating fodler if not exist for saving data
        if not os.path.exists(STATES_PICKLE):
            os.mkdir(STATES_PICKLE)
            logger.info(f"{STATES_PICKLE} folder created")

        # saving states
        try:
            f = open(os.path.join(STATES_PICKLE, f'states_{self.__class__.__name__}.pickle'), 'wb+')
            pickle.dump(resolved, f)
            f.close()
            logger.info(f"States saved for {self.__class__.__name__}")
        except FileNotFoundError:
            logger.error(f"States folder not found for {self.__class__.__name__}")

    def save_user_data(self, dispatcher):
        if not os.path.exists(USERDATA_PICKLE):
            os.mkdir(USERDATA_PICKLE)
            logger.info(f"{USERDATA_PICKLE} folder created")

        for handler in self.menus:
            handler.save_user_data(dispatcher)

        try:
            f = open(os.path.join(USERDATA_PICKLE, f'userdata_{self.__class__.__name__}.pickle'), 'wb+')

            filtered_user_data = defaultdict(dict)
            for chat_id, data in dispatcher.user_data.items():  # chat_ids
                if '_' in dispatcher.user_data[chat_id]:
                    del dispatcher.user_data[chat_id]['_']

                one_user_data = {}
                for key, value in data.items():  # user_datas
                    if key != 'database_menu':
                        one_user_data.update({key: value})
                    else:
                        if isinstance(value, dict):
                            database_menu = {}
                            for k, v in value.items():
                                if k != 'tpd':
                                    database_menu.update({k: v})
                            one_user_data.update({key: database_menu})
                        else:
                            one_user_data.update({key: value})

                filtered_user_data.update({chat_id: one_user_data})

            pickle.dump(filtered_user_data, f)
            f.close()
            logger.info(f"UserData saved for {self.__class__.__name__}")

        except FileNotFoundError:
            logger.error(f"UserData folder not found for {self.__class__.__name__}")

    def load_data(self, dispatcher):
        for menu in self.menus:
            menu.load_data(dispatcher)
        logger_text = ""

        try:
            f = open(os.path.join(STATES_PICKLE, f'states_{self.__class__.__name__}.pickle'), 'rb')
            self.handler.conversations = pickle.load(f)
            f.close()
            logger_text += "States"
        except TypeError as e:
            logger.error(str(e))
        except (FileNotFoundError, EOFError):
            logger.error(f"StatesData file not found or empty for {self.__class__.__name__}")

        try:
            f = open(os.path.join(USERDATA_PICKLE, f'userdata_{self.__class__.__name__}.pickle'), 'rb')
            dispatcher.user_data = pickle.load(f)
            for key in dispatcher.user_data:
                dispatcher.user_data[key]['restarted'] = True
                if 'user' in dispatcher.user_data[key]:
                    DBSession.add(dispatcher.user_data[key]['user'])
                    dispatcher.user_data[key]['_'] = generate_underscore(dispatcher.user_data[key]['user'])

            f.close()
            if logger_text:
                logger_text += " and UserData"
            else:
                logger_text += "UserData"
        except TypeError as e:
            logger.error(str(e))
        except (FileNotFoundError, EOFError):
            logger.error(f"UserData file not found or empty for {self.__class__.__name__}")

        if logger_text:
            logger.info(f"{logger_text} loaded for {self.__class__.__name__}")

    def get_handler(self):
        raise NotImplementedError()

    def clear_messages(self, user_data):
        """
        This function tries to delete all messages added to user_data['to_delete']
        :param user_data:
        :return: None
        """
        if 'to_delete' in user_data:
            user_data['to_delete'] = set(user_data['to_delete'])
            for m_id in user_data['to_delete']:
                try:
                    self.bot.delete_message(chat_id=user_data['user'].chat_id, message_id=m_id)
                except (BadRequest, AttributeError) as e:
                    logger.error(f"Can't delete message with id {m_id}, because {str(e)}")

        user_data['to_delete'] = []

    def add_to_db(self, user_data, objects):
        _ = user_data['_'] if '_' in user_data else gettext.gettext

        try:
            if not isinstance(objects, list):
                objects = [objects]

            for object in objects:
                DBSession.add(object)
            DBSession.commit()
            user_data['user'] = DBSession.query(User).filter(User.id == user_data['user'].id).first()
            return True
        except (SQLAlchemyError, IntegrityError) as e:
            DBSession.rollback()
            logger.critical(f"Database error: {str(e)}")
            self.bot.send_message(chat_id=user_data['user'].chat_id, text=_("Database error, email about this error to support@botman.com.ua"))
            return False

    def remove_from_db(self, user_data, *objects):
        _ = user_data['_'] if '_' in user_data else gettext.gettext

        try:
            for object in objects:
                DBSession.delete(object)
            DBSession.commit()
            user_data['user'] = DBSession.query(User).filter(User.id == user_data['user'].id).first()
            return True
        except (SQLAlchemyError, IntegrityError) as e:
            DBSession.rollback()
            logger.critical(f"Database error: {str(e)}")
            self.bot.send_message(chat_id=user_data['user'].chat_id, text=_("Database error, email about this error to support@botman.com.ua"))
            return False

    def conv_fallback(self, user_data):
        """
        This function was called when some error in bot occurs.
        :param user_data:
        :return: -1
        """
        _ = user_data['_'] if '_' in user_data else gettext.translation('base', os.path.join(PROJECT_ROOT, '..', 'locales'), languages=['en']).gettext
        self.clear_messages(user_data)

        if 'markup' in user_data and user_data['markup']:
            self.bot.send_message(chat_id=user_data['user'].chat_id, text=_("Something went wrong, try again later."), reply_markup=user_data['markup'])
            del user_data['markup']
        else:
            self.bot.send_message(chat_id=user_data['user'].chat_id, text=_("Something went wrong, try again later."))

        return ConversationHandler.END

    def unknown_command(self, state):
        def unk_c_fun(bot, update, user_data=None):
            bot.send_message(chat_id=update.message.chat_id, text="Sorry, unknown command.")
            return state

        return unk_c_fun
