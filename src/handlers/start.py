import datetime
import enum
import hashlib
import logging
import uuid

import formencode
from emoji import emojize
from formencode import validators
from telegram import ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CommandHandler, ConversationHandler, MessageHandler, filters, CallbackQueryHandler

from src.handlers.base import BaseMenu
from src.handlers.database import DatabaseMenu
from src.lib.translations import generate_underscore, get_langs
from src.models import DBSession, User, Languages, PasswordDatabase
from src.settings import SECRET

logger = logging.getLogger(__name__)


class StartStates(enum.Enum):
    ACTION = 1
    LANGUAGE = 2
    CREATE_DATABASE = 3
    CREATE_DATABASE_PASSWORD = 4
    CHOOSE_DATABASE = 5


class StartMenu(BaseMenu):

    @property
    def language_markup(self):
        return ReplyKeyboardMarkup([[emojize("English:United_States:"), emojize("Русский:Russia:")]], resize_keyboard=True, one_time_keyboard=True)

    def entry(self, bot, update, user_data):
        user = DBSession.query(User).filter(User.chat_id == update.message.chat_id).first()
        user_data.clear()
        user_data['start_menu'] = {}
        user_data['interface'] = None

        if user is None:
            # create new
            user = User()
            user.name = f"{update.message.from_user.first_name if update.message.from_user.first_name else ''} {update.message.from_user.last_name if update.message.from_user.last_name else ''}"
            user.username = f"@{update.message.from_user.username if update.message.from_user.username else ''}"
            user.join_date = datetime.datetime.now()
            user.chat_id = update.message.chat_id
        else:
            # update user
            user.active = True
            user.name = f"{update.message.from_user.first_name if update.message.from_user.first_name else ''} {update.message.from_user.last_name if update.message.from_user.last_name else ''}"
            user.username = f"@{update.message.from_user.username if update.message.from_user.username else ''}"
            user.chat_id = update.message.chat_id

        user_data['user'] = user

        # add to db
        if not self.add_to_db(user_data, user):
            return self.conv_fallback(user_data)

        user_data['interface'] = bot.send_message(chat_id=user.chat_id, text="Choose your language", reply_markup=self.language_markup)
        return StartStates.LANGUAGE

    def choose_language(self, bot, update, user_data):
        user = user_data['user']
        text = update.message.text
        try:
            val = validators.OneOf([emojize("English:United_States:"), emojize("Русский:Russia:")])
            value = val.to_python(text)
            if value == emojize("English:United_States:"):
                user.lang = Languages.en
            elif value == emojize("Русский:Russia:"):
                user.lang = Languages.ru
            else:
                raise formencode.Invalid("Some extra error", value, None)
            user_data['interface'].delete()
            user_data['interface'] = None

            user_data['_'] = generate_underscore(user)
            _ = user_data['_']

            if not self.add_to_db(user_data, user):
                return self.conv_fallback(user_data)

            bot.send_message(chat_id=user.chat_id, text=_("Read /help for aditional info."))

            if not user.password_databases:
                bot.send_message(chat_id=user.chat_id, text=_("Welcome, you new here.") + _("Please send me your new password database name:"))
                return StartStates.CREATE_DATABASE
            else:
                user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Welcome, choose your password database:"), reply_markup=self.databases_markup(user_data))
                user_data['start_menu']['interface'] = user_data['interface']
                return StartStates.CHOOSE_DATABASE

        except formencode.Invalid:
            bot.send_message(chat_id=user.chat_id, text=_("Wrong input."))
            return StartStates.LANGUAGE

    def create_database(self, bot, update, user_data):
        _ = user_data['_']
        user = user_data['user']
        if not update.message:
            bot.send_message(chat_id=user.chat_id, text=_("Please send me your new password database name:"))
            return StartStates.CREATE_DATABASE

        text = update.message.text
        try:
            val = validators.UnicodeString()
            value = val.to_python(text)

            pd = PasswordDatabase(user_id=user.id, name=value)
            user_data['start_menu']['new_pd'] = pd

            if not self.add_to_db(user_data, pd):
                return self.conv_fallback(user_data)

            bot.send_message(chat_id=user.chat_id, text=_("Input your new password for this database:"))
            return StartStates.CREATE_DATABASE_PASSWORD

        except formencode.Invalid:
            bot.send_message(chat_id=user.chat_id, text=_("Wrong input."))
            return StartStates.CREATE_DATABASE

    def create_database_password(self, bot, update, user_data):
        _ = user_data['_']
        user = user_data['user']
        text = update.message.text
        try:
            val = validators.UnicodeString()
            value = val.to_python(text)

            hashed_password = hashlib.sha512((value + SECRET).encode('utf-8')).hexdigest()

            pd = user_data['start_menu']['new_pd']
            pd.master_password = hashed_password

            if not self.add_to_db(user_data, pd):
                return self.conv_fallback(user_data)

            bot.send_message(chat_id=user.chat_id, text=_("Your password database created."))
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Welcome, choose your password database:"), reply_markup=self.databases_markup(user_data))
            user_data['start_menu']['interface'] = user_data['interface']
            return StartStates.CHOOSE_DATABASE

        except formencode.Invalid:
            bot.send_message(chat_id=user.chat_id, text=_("Wrong input."))
            return StartStates.CREATE_DATABASE_PASSWORD

    def to_choose_database(self, bot, update):
        return StartStates.CHOOSE_DATABASE

    def import_error(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        user_data['interface'].delete()

        user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("For importing you mast log into database first."))
        user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Welcome, choose your password database:"), reply_markup=self.databases_markup(user_data))
        return StartStates.CHOOSE_DATABASE

    def get_handler(self):

        database_menu = DatabaseMenu()
        self.menus.append(database_menu)
        # create_new_database_handlers = []
        # for _ in get_langs():

        handler = ConversationHandler(entry_points=[CommandHandler('start', self.entry, pass_user_data=True)],
                                      states={
                                          StartStates.LANGUAGE: [MessageHandler(filters.Filters.text, self.choose_language, pass_user_data=True)],
                                          StartStates.CREATE_DATABASE: [MessageHandler(filters.Filters.text, self.create_database, pass_user_data=True)],
                                          StartStates.CREATE_DATABASE_PASSWORD: [MessageHandler(filters.Filters.text, self.create_database_password, pass_user_data=True)],
                                          StartStates.CHOOSE_DATABASE: [CallbackQueryHandler(self.create_database, pattern=r"^new_database$", pass_user_data=True),
                                                                        database_menu.handler,
                                                                        CommandHandler('import', self.import_error, pass_user_data=True),
                                                                        MessageHandler(filters.Filters.all, self.to_choose_database)],
                                      },
                                      fallbacks=[MessageHandler(filters.Filters.all, self.unknown_command(-1), pass_user_data=True)],
                                      allow_reentry=True)

        return handler
