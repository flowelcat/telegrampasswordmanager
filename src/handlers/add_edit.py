import enum
import hashlib
from random import sample as rsample

import formencode
from emoji import emojize
from formencode import validators
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, filters

from src.handlers.base import BaseMenu
from src.lib.crypt import AESCipher
from src.models import DatabaseEntry, TelegramPasswordDatabase, DatabaseGroup, DBSession
from src.settings import SECRET


class AddEditStates(enum.Enum):
    ACTION = 1
    CREATE_WHAT = 2
    INPUT_VALUES_ENTRY = 3
    ENTRY_SAVE = 4
    INPUT_VALUES_GROUP = 5
    GROUP_SAVE = 6


class AddEditMenu(BaseMenu):
    def entry(self, bot, update, user_data):
        _ = user_data['_']

        reply_markup = InlineKeyboardMarkup([[InlineKeyboardButton(_("Entry"), callback_data='create_entry'), InlineKeyboardButton(_("Group"), callback_data='create_group')],
                                             [InlineKeyboardButton(_("Back"), callback_data='back')]])

        user_data['interface'].edit_text(text=_("What you want create?"), reply_markup=reply_markup)

        user_data['addedit_menu'] = {}
        user_data['addedit_menu']['title'] = None
        user_data['addedit_menu']['username'] = None
        user_data['addedit_menu']['password'] = None
        user_data['addedit_menu']['url'] = None
        user_data['addedit_menu']['notes'] = None

        user_data['addedit_menu']['g_title'] = None
        user_data['addedit_menu']['g_notes'] = None

        user_data['addedit_menu']['input_active'] = 0

        bot.answer_callback_query(update.callback_query.id)

        return AddEditStates.CREATE_WHAT

    def send_message_entry(self, user_data):
        user = user_data['user']
        _ = user_data['_']

        input_active = user_data['addedit_menu']['input_active']

        message_text = f"_______{_('Entry')}_______\n"

        message_text += (emojize(':play_button:️') if input_active == 0 else '      ') + _("Title") + ':' + str(user_data['addedit_menu']['title']) + '\n'
        message_text += (emojize(':play_button:️') if input_active == 1 else '      ') + _("Username") + ':' + str(user_data['addedit_menu']['username']) + '\n'
        message_text += (emojize(':play_button:️') if input_active == 2 else '      ') + _("Password") + ':' + str(user_data['addedit_menu']['password']) + '\n'
        message_text += (emojize(':play_button:️') if input_active == 3 else '      ') + _("URL") + ':' + str(user_data['addedit_menu']['url']) + '\n'
        message_text += (emojize(':play_button:️') if input_active == 4 else '      ') + _("Notes") + ':' + str(user_data['addedit_menu']['notes'])

        buttons = []
        buttons.append([InlineKeyboardButton(emojize(":reverse_button:"), callback_data="input_previous"),
                        InlineKeyboardButton(emojize(":up_arrow:"), callback_data="back"),
                        InlineKeyboardButton(emojize(":white_heavy_check_mark:"), callback_data="input_save"),
                        InlineKeyboardButton(emojize(":play_button:"), callback_data="input_next")
                        ])

        buttons.append([InlineKeyboardButton(_("Title"), callback_data='input_title')])
        buttons.append([InlineKeyboardButton(_("Username"), callback_data='input_username')])
        buttons.append([InlineKeyboardButton(_("Password"), callback_data='input_password'), InlineKeyboardButton(_("Generate"), callback_data='generate_password')])
        buttons.append([InlineKeyboardButton(_("URL"), callback_data='input_url')])
        buttons.append([InlineKeyboardButton(_("Notes"), callback_data='input_notes')])

        if 'interface' in user_data and user_data['interface'] is not None:
            user_data['interface'].edit_text(text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        else:
            user_data['interface'] = self.bot.send_message(chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
            user_data['addedit_menu']['interface'] = user_data['interface']
            user_data['addedit_menu']['markup'] = InlineKeyboardMarkup(buttons)

    def send_message_group(self, user_data):
        user = user_data['user']
        _ = user_data['_']

        input_active = user_data['addedit_menu']['input_active']

        message_text = f"_______{_('Group')}_______\n"

        message_text += (emojize(':play_button:️') if input_active == 0 else '      ') + _("Title") + ':' + str(user_data['addedit_menu']['g_title']) + '\n'
        message_text += (emojize(':play_button:️') if input_active == 1 else '      ') + _("Notes") + ':' + str(user_data['addedit_menu']['g_notes'])

        buttons = []
        buttons.append([InlineKeyboardButton(emojize(":reverse_button:"), callback_data="input_previous"),
                        InlineKeyboardButton(emojize(":up_arrow:"), callback_data="back"),
                        InlineKeyboardButton(emojize(":white_heavy_check_mark:"), callback_data="input_save"),
                        InlineKeyboardButton(emojize(":play_button:"), callback_data="input_next")
                        ])

        buttons.append([InlineKeyboardButton(_("Title"), callback_data='input_title')])
        buttons.append([InlineKeyboardButton(_("Notes"), callback_data='input_notes')])

        if 'interface' in user_data and user_data['interface'] is not None:
            user_data['interface'].edit_text(text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        else:
            user_data['interface'] = self.bot.send_message(chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
            user_data['addedit_menu']['interface'] = user_data['interface']
            user_data['addedit_menu']['markup'] = InlineKeyboardMarkup(buttons)

    def create_entry(self, bot, update, user_data):
        _ = user_data['_']

        user_data['addedit_menu']['input_active'] = 0
        self.send_message_entry(user_data)

        bot.answer_callback_query(update.callback_query.id)

        return AddEditStates.INPUT_VALUES_ENTRY

    def create_group(self, bot, update, user_data):
        _ = user_data['_']

        user_data['addedit_menu']['input_active'] = 0
        self.send_message_group(user_data)

        bot.answer_callback_query(update.callback_query.id)

        return AddEditStates.INPUT_VALUES_GROUP

    def change_input(self, bot, update, user_data):
        data = update.callback_query.data.replace('input_', '')

        if data == 'title':
            input_active = 0
        elif data == 'username':
            input_active = 1
        elif data == 'password':
            input_active = 2
        elif data == 'url':
            input_active = 3
        elif data == 'notes':
            input_active = 4
        else:
            input_active = 0

        user_data['addedit_menu']['input_active'] = input_active
        bot.answer_callback_query(update.callback_query.id)

        self.send_message_entry(user_data)
        return AddEditStates.INPUT_VALUES_ENTRY

    def change_input_group(self, bot, update, user_data):
        data = update.callback_query.data.replace('input_', '')

        if data == 'title':
            input_active = 0
        elif data == 'notes':
            input_active = 1
        else:
            input_active = 0

        user_data['addedit_menu']['input_active'] = input_active
        bot.answer_callback_query(update.callback_query.id)

        self.send_message_group(user_data)
        return AddEditStates.INPUT_VALUES_GROUP

    def prev_input(self, bot, update, user_data):
        input_active = user_data['addedit_menu']['input_active']
        if input_active <= 0:
            user_data['addedit_menu']['input_active'] = 4
        else:
            user_data['addedit_menu']['input_active'] -= 1

        bot.answer_callback_query(update.callback_query.id)
        self.send_message_entry(user_data)

        return AddEditStates.INPUT_VALUES_ENTRY

    def prev_input_group(self, bot, update, user_data):
        input_active = user_data['addedit_menu']['input_active']
        if input_active <= 0:
            user_data['addedit_menu']['input_active'] = 1
        else:
            user_data['addedit_menu']['input_active'] -= 1

        bot.answer_callback_query(update.callback_query.id)
        self.send_message_group(user_data)

        return AddEditStates.INPUT_VALUES_GROUP

    def next_input(self, bot, update, user_data):
        input_active = user_data['addedit_menu']['input_active']
        if input_active >= 4:
            user_data['addedit_menu']['input_active'] = 0
        else:
            user_data['addedit_menu']['input_active'] += 1

        bot.answer_callback_query(update.callback_query.id)
        self.send_message_entry(user_data)

        return AddEditStates.INPUT_VALUES_ENTRY

    def next_input_group(self, bot, update, user_data):
        input_active = user_data['addedit_menu']['input_active']
        if input_active >= 1:
            user_data['addedit_menu']['input_active'] = 0
        else:
            user_data['addedit_menu']['input_active'] += 1

        bot.answer_callback_query(update.callback_query.id)
        self.send_message_group(user_data)

        return AddEditStates.INPUT_VALUES_GROUP

    def generate_password(self, bot, update, user_data):
        s = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        gen_password = "".join(rsample(s, 8))
        user_data['addedit_menu']['password'] = gen_password

        if user_data['addedit_menu']['title'] is None:
            user_data['addedit_menu']['input_active'] = 0
        elif user_data['addedit_menu']['username'] is None:
            user_data['addedit_menu']['input_active'] = 1
        elif user_data['addedit_menu']['password'] is None:
            user_data['addedit_menu']['input_active'] = 2
        elif user_data['addedit_menu']['url'] is None:
            user_data['addedit_menu']['input_active'] = 3
        elif user_data['addedit_menu']['notes'] is None:
            user_data['addedit_menu']['input_active'] = 4
        else:
            user_data['addedit_menu']['input_active'] = 0

        bot.answer_callback_query(update.callback_query.id)
        self.send_message_entry(user_data)

        return AddEditStates.INPUT_VALUES_ENTRY

    def input_enter(self, bot, update, user_data):
        text = update.message.text

        input_active = user_data['addedit_menu']['input_active']
        if input_active == 0:
            user_data['addedit_menu']['title'] = text
        elif input_active == 1:
            user_data['addedit_menu']['username'] = text
        elif input_active == 2:
            user_data['addedit_menu']['password'] = text
        elif input_active == 3:
            user_data['addedit_menu']['url'] = text
        elif input_active == 4:
            user_data['addedit_menu']['notes'] = text

        if input_active >= 4:
            user_data['addedit_menu']['input_active'] = 0
        else:
            user_data['addedit_menu']['input_active'] += 1

        self.send_message_entry(user_data)

        return AddEditStates.INPUT_VALUES_ENTRY

    def input_enter_group(self, bot, update, user_data):
        text = update.message.text

        input_active = user_data['addedit_menu']['input_active']
        if input_active == 0:
            user_data['addedit_menu']['g_title'] = text
        elif input_active == 1:
            user_data['addedit_menu']['g_notes'] = text

        if input_active >= 1:
            user_data['addedit_menu']['input_active'] = 0
        else:
            user_data['addedit_menu']['input_active'] += 1

        self.send_message_group(user_data)

        return AddEditStates.INPUT_VALUES_GROUP

    def back_to_add(self, bot, update, user_data):
        self.send_message_entry(user_data)

        bot.answer_callback_query(update.callback_query.id)
        return AddEditStates.INPUT_VALUES_ENTRY

    def back_to_add_group(self, bot, update, user_data):
        self.send_message_group(user_data)

        bot.answer_callback_query(update.callback_query.id)
        return AddEditStates.INPUT_VALUES_GROUP

    def back(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        self.send_message_view(user_data)

        bot.answer_callback_query(update.callback_query.id)

        return ConversationHandler.END

    def ask_password(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        bot.answer_callback_query(update.callback_query.id)
        if user_data['addedit_menu']['title'] is not None and user_data['addedit_menu']['password'] is not None:
            user_data['interface'].delete()
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text="Enter master password for saving this entry:", reply_markup=self.back_markup(user_data))
            return AddEditStates.ENTRY_SAVE
        else:
            user_data['interface'].delete()
            user_data['interface'] = None

            bot.send_message(chat_id=user.chat_id, text=_("Please fill title and password fields"))
            self.send_message_entry(user_data)
            return AddEditStates.INPUT_VALUES_ENTRY

    def ask_password_group(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        bot.answer_callback_query(update.callback_query.id)
        if user_data['addedit_menu']['g_title'] is not None:
            user_data['interface'].delete()
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text="Enter master password for saving this group:", reply_markup=self.back_markup(user_data))
            return AddEditStates.GROUP_SAVE
        else:
            user_data['interface'].delete()
            user_data['interface'] = None

            bot.send_message(chat_id=user.chat_id, text=_("Please fill title field"))
            self.send_message_group(user_data)
            return AddEditStates.INPUT_VALUES_GROUP

    def entry_save(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']
        text = update.message.text
        try:
            val = validators.String()
            value = val.to_python(text)

            hashed_password = hashlib.sha512((value + SECRET).encode('utf-8')).hexdigest()
            if user_data['database_menu']['database'].master_password == hashed_password:
                if 'active_group' not in user_data or user_data['active_group'] is None:
                    group_id = None
                else:
                    group_id = user_data['active_group'].id

                key = "{: <32}".format(value).encode("utf-8")

                aes_c = AESCipher(key)

                c_title = aes_c.encrypt(user_data['addedit_menu']['title'])
                c_username = aes_c.encrypt(user_data['addedit_menu']['username'])
                c_password = aes_c.encrypt(user_data['addedit_menu']['password'])
                c_url = aes_c.encrypt(user_data['addedit_menu']['url'])
                c_notes = aes_c.encrypt(user_data['addedit_menu']['notes'])

                entry = DatabaseEntry(database_id=user_data['database_menu']['database'].id,
                                      group_id=group_id,
                                      title=c_title,
                                      username=c_username,
                                      password=c_password,
                                      url=c_url,
                                      notes=c_notes)

                if not self.add_to_db(user_data, entry):
                    return self.conv_fallback(user_data)

                #  reload db to memory
                user_data['database_menu']['tpd'] = TelegramPasswordDatabase(key, user_data['database_menu']['database'].id)
                if 'active_group' in user_data and user_data['active_group']:
                    user_data['active_group'] = user_data['database_menu']['tpd'].get_group(user_data['active_group'].id)

                user_data['interface'].edit_text(text="Entry added")
                user_data['interface'] = None

                self.send_message_view(user_data)

                return ConversationHandler.END
            else:
                raise formencode.Invalid("Wrong password", value, None)

        except formencode.Invalid:
            user_data['interface'].edit_reply_markup()
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Wrong password, try again"), reply_markup=self.back_markup(user_data))
            return AddEditStates.ENTRY_SAVE

    def group_save(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']
        text = update.message.text
        try:
            val = validators.UnicodeString()
            value = val.to_python(text)

            hashed_password = hashlib.sha512((value + SECRET).encode('utf-8')).hexdigest()
            if user_data['database_menu']['database'].master_password == hashed_password:
                if 'active_group' not in user_data or user_data['active_group'] is None:
                    group_id = None
                else:
                    group_id = user_data['active_group'].id

                key = "{: <32}".format(value).encode("utf-8")

                aes_c = AESCipher(key)

                c_title = aes_c.encrypt(user_data['addedit_menu']['g_title'])
                c_notes = aes_c.encrypt(user_data['addedit_menu']['g_notes'])

                group = DatabaseGroup(database_id=user_data['database_menu']['database'].id,
                                      parent_group_id=group_id,
                                      title=c_title,
                                      notes=c_notes)

                if not self.add_to_db(user_data, group):
                    return self.conv_fallback(user_data)

                #  reload db to memory
                user_data['database_menu']['tpd'] = TelegramPasswordDatabase(key, user_data['database_menu']['database'].id)
                if 'active_group' in user_data and user_data['active_group']:
                    user_data['active_group'] = user_data['database_menu']['tpd'].get_group(user_data['active_group'].id)

                user_data['interface'].edit_text(text="Group added")
                user_data['interface'] = None

                self.send_message_view(user_data)

                return ConversationHandler.END
            else:
                raise formencode.Invalid("Wrong password", value, None)

        except formencode.Invalid:
            user_data['interface'].edit_reply_markup()
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Wrong password, try again"), reply_markup=self.back_markup(user_data))
            return AddEditStates.GROUP_SAVE

    def get_handler(self):

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern=r'^create_new$', pass_user_data=True)],
                                      states={
                                          AddEditStates.CREATE_WHAT: [CallbackQueryHandler(self.back, pattern=r'^back$', pass_user_data=True),
                                                                      CallbackQueryHandler(self.create_entry, pattern=r'^create_entry$', pass_user_data=True),
                                                                      CallbackQueryHandler(self.create_group, pattern=r'^create_group$', pass_user_data=True)],

                                          AddEditStates.INPUT_VALUES_ENTRY: [CallbackQueryHandler(self.back, pattern=r'^back$', pass_user_data=True),
                                                                             CallbackQueryHandler(self.change_input, pattern=r'^input_(title|username|password|url|notes)$', pass_user_data=True),
                                                                             CallbackQueryHandler(self.next_input, pattern=r'^input_next', pass_user_data=True),
                                                                             CallbackQueryHandler(self.prev_input, pattern=r'^input_previous', pass_user_data=True),
                                                                             CallbackQueryHandler(self.ask_password, pattern=r'^input_save', pass_user_data=True),
                                                                             CallbackQueryHandler(self.generate_password, pattern=r'^generate_password$', pass_user_data=True),
                                                                             MessageHandler(filters.Filters.text, self.input_enter, pass_user_data=True)],
                                          AddEditStates.ENTRY_SAVE: [MessageHandler(filters.Filters.text, self.entry_save, pass_user_data=True),
                                                                     CallbackQueryHandler(self.back_to_add, pattern=r'^back$', pass_user_data=True)],
                                          AddEditStates.INPUT_VALUES_GROUP: [CallbackQueryHandler(self.back, pattern=r'^back$', pass_user_data=True),
                                                                             CallbackQueryHandler(self.change_input_group, pattern=r'^input_(title|notes)$', pass_user_data=True),
                                                                             CallbackQueryHandler(self.next_input_group, pattern=r'^input_next', pass_user_data=True),
                                                                             CallbackQueryHandler(self.prev_input_group, pattern=r'^input_previous', pass_user_data=True),
                                                                             CallbackQueryHandler(self.ask_password_group, pattern=r'^input_save', pass_user_data=True),
                                                                             MessageHandler(filters.Filters.text, self.input_enter_group, pass_user_data=True)],
                                          AddEditStates.GROUP_SAVE: [MessageHandler(filters.Filters.text, self.group_save, pass_user_data=True),
                                                                     CallbackQueryHandler(self.back_to_add_group, pattern=r'^back$', pass_user_data=True)],
                                      },
                                      fallbacks=[MessageHandler(filters.Filters.all, self.unknown_command(-1), pass_user_data=True)],
                                      allow_reentry=True)

        return handler


class EditMenu(AddEditMenu):
    def entry(self, bot, update, user_data):
        _ = user_data['_']

        bot.answer_callback_query(update.callback_query.id)
        data = update.callback_query.data

        user_data['addedit_menu'] = {}
        user_data['addedit_menu']['input_active'] = 0

        if data.startswith('edit_group_'):
            group = user_data['database_menu']['tpd'].get_group(int(data.replace('edit_group_', '')))

            user_data['addedit_menu']['g_title'] = group.title
            user_data['addedit_menu']['g_notes'] = group.notes
            user_data['addedit_menu']['g_id'] = group.id
            self.send_message_group(user_data)
            return AddEditStates.INPUT_VALUES_GROUP

        elif data.startswith('edit_entry_'):
            entry = user_data['database_menu']['tpd'].get_entry(int(data.replace('edit_entry_', '')))

            user_data['addedit_menu']['title'] = entry.title
            user_data['addedit_menu']['username'] = entry.username
            user_data['addedit_menu']['password'] = entry.password
            user_data['addedit_menu']['url'] = entry.url
            user_data['addedit_menu']['notes'] = entry.notes
            user_data['addedit_menu']['id'] = entry.id
            self.send_message_entry(user_data)
            return AddEditStates.INPUT_VALUES_ENTRY

        else:
            return ConversationHandler.END

    def entry_save(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']
        text = update.message.text
        try:
            val = validators.UnicodeString()
            value = val.to_python(text)

            hashed_password = hashlib.sha512((value + SECRET).encode('utf-8')).hexdigest()
            if user_data['database_menu']['database'].master_password == hashed_password:

                key = "{: <32}".format(value).encode("utf-8")

                aes_c = AESCipher(key)

                c_title = aes_c.encrypt(user_data['addedit_menu']['title'])
                c_username = aes_c.encrypt(user_data['addedit_menu']['username'])
                c_password = aes_c.encrypt(user_data['addedit_menu']['password'])
                c_url = aes_c.encrypt(user_data['addedit_menu']['url'])
                c_notes = aes_c.encrypt(user_data['addedit_menu']['notes'])

                entry = DBSession.query(DatabaseEntry).filter(DatabaseEntry.id == user_data['addedit_menu']['id']).first()
                entry.title = c_title
                entry.username = c_username
                entry.password = c_password
                entry.url = c_url
                entry.notes = c_notes

                if not self.add_to_db(user_data, entry):
                    return self.conv_fallback(user_data)

                #  reload db to memory
                user_data['database_menu']['tpd'] = TelegramPasswordDatabase(key, user_data['database_menu']['database'].id)

                user_data['interface'].edit_text(text="Entry saved")
                user_data['interface'] = None

                self.send_message_view(user_data)

                return ConversationHandler.END
            else:
                raise formencode.Invalid("Wrong password", value, None)

        except formencode.Invalid:
            user_data['interface'].edit_reply_markup()
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Wrong password, try again"), reply_markup=self.back_markup(user_data))
            return AddEditStates.INPUT_VALUES_ENTRY

    def group_save(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']
        text = update.message.text
        try:
            val = validators.UnicodeString()
            value = val.to_python(text)

            hashed_password = hashlib.sha512((value + SECRET).encode('utf-8')).hexdigest()
            if user_data['database_menu']['database'].master_password == hashed_password:

                key = "{: <32}".format(value).encode("utf-8")

                aes_c = AESCipher(key)

                c_title = aes_c.encrypt(user_data['addedit_menu']['g_title'])
                c_notes = aes_c.encrypt(user_data['addedit_menu']['g_notes'])

                group = DBSession.query(DatabaseGroup).filter(DatabaseGroup.id == user_data['addedit_menu']['g_id']).first()
                group.title = c_title
                group.notes = c_notes

                if not self.add_to_db(user_data, group):
                    return self.conv_fallback(user_data)

                #  reload db to memory
                user_data['database_menu']['tpd'] = TelegramPasswordDatabase(key, user_data['database_menu']['database'].id)
                if 'active_group' in user_data and user_data['active_group']:
                    user_data['active_group'] = user_data['database_menu']['tpd'].get_group(user_data['active_group'].id)

                user_data['interface'].edit_text(text="Group saved")
                user_data['interface'] = None

                self.send_message_view(user_data)

                return ConversationHandler.END
            else:
                raise formencode.Invalid("Wrong password", value, None)

        except formencode.Invalid:
            user_data['interface'].edit_reply_markup()
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Wrong password, try again"), reply_markup=self.back_markup(user_data))
            return AddEditStates.INPUT_VALUES_GROUP

    def get_handler(self):

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern=r'^edit_(group|entry)_\d+$', pass_user_data=True)],
                                      states={
                                          AddEditStates.INPUT_VALUES_ENTRY: [CallbackQueryHandler(self.back, pattern=r'^back$', pass_user_data=True),
                                                                             CallbackQueryHandler(self.change_input, pattern=r'^input_(title|username|password|url|notes)$', pass_user_data=True),
                                                                             CallbackQueryHandler(self.next_input, pattern=r'^input_next', pass_user_data=True),
                                                                             CallbackQueryHandler(self.prev_input, pattern=r'^input_previous', pass_user_data=True),
                                                                             CallbackQueryHandler(self.ask_password, pattern=r'^input_save', pass_user_data=True),
                                                                             CallbackQueryHandler(self.generate_password, pattern=r'^generate_password$', pass_user_data=True),
                                                                             MessageHandler(filters.Filters.text, self.input_enter, pass_user_data=True)],
                                          AddEditStates.ENTRY_SAVE: [MessageHandler(filters.Filters.text, self.entry_save, pass_user_data=True),
                                                                     CallbackQueryHandler(self.back_to_add, pattern=r'^back$', pass_user_data=True)],
                                          AddEditStates.INPUT_VALUES_GROUP: [CallbackQueryHandler(self.back, pattern=r'^back$', pass_user_data=True),
                                                                             CallbackQueryHandler(self.change_input_group, pattern=r'^input_(title|notes)$', pass_user_data=True),
                                                                             CallbackQueryHandler(self.next_input_group, pattern=r'^input_next', pass_user_data=True),
                                                                             CallbackQueryHandler(self.prev_input_group, pattern=r'^input_previous', pass_user_data=True),
                                                                             CallbackQueryHandler(self.ask_password_group, pattern=r'^input_save', pass_user_data=True),
                                                                             MessageHandler(filters.Filters.text, self.input_enter_group, pass_user_data=True)],
                                          AddEditStates.GROUP_SAVE: [MessageHandler(filters.Filters.text, self.group_save, pass_user_data=True),
                                                                     CallbackQueryHandler(self.back_to_add_group, pattern=r'^back$', pass_user_data=True)],
                                      },
                                      fallbacks=[MessageHandler(filters.Filters.all, self.unknown_command(-1), pass_user_data=True)],
                                      allow_reentry=True)

        return handler
