import enum
import logging

from emoji import emojize
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import ConversationHandler, MessageHandler, filters, CallbackQueryHandler, CommandHandler

from src.handlers.add_edit import EditMenu
from src.handlers.base import BaseMenu
from src.models import DBSession, DatabaseGroup, TelegramPasswordGroup
from src.settings import ITEMS_PER_PAGE

logger = logging.getLogger(__name__)


class GroupStates(enum.Enum):
    ACTION = 1
    DELETE_CONFIRMATION = 2


class GroupMenu(BaseMenu):

    def entry(self, bot, update, user_data):
        _ = user_data['_']
        user = user_data['user']

        data = int(update.callback_query.data.replace("group_", ""))

        group = user_data['database_menu']['tpd'].get_group(data)

        user_data['group_menu'] = {}
        user_data['active_group'] = group

        self.send_message_view(user_data)

        bot.answer_callback_query(update.callback_query.id)
        return GroupStates.ACTION

    def delete(self, bot, update, user_data):
        _ = user_data['_']
        user = user_data['user']

        reply_markup = InlineKeyboardMarkup([[InlineKeyboardButton(emojize(":white_heavy_check_mark: ") + _("Yes") + emojize(" :white_heavy_check_mark:"), callback_data='delete_yes')],
                                             [InlineKeyboardButton(emojize(":no_entry:️ ") + _("No") + emojize(" :no_entry:️"), callback_data='delete_no')]])

        user_data['interface'] = user_data['interface'].edit_text(text=_("You really want to delete {name} group?").format(name=user_data['active_group'].title), reply_markup=reply_markup)

        bot.answer_callback_query(update.callback_query.id)
        return GroupStates.DELETE_CONFIRMATION

    def delete_confirmation(self, bot, update, user_data):
        _ = user_data['_']
        user = user_data['user']
        data = update.callback_query.data
        if data == 'delete_yes':
            group = DBSession.query(DatabaseGroup).filter(DatabaseGroup.id == user_data['active_group'].id).first()

            if not self.remove_from_db(user_data, group):
                return self.conv_fallback(user_data)

            user_data['interface'].edit_text(text="Group deleted")
            user_data['interface'] = None

            user_data['database_menu']['tpd'].delete_group(user_data['active_group'])

            if user_data['active_group'].group_id is not None:
                user_data['active_group'] = user_data['database_menu']['tpd'].get_group(user_data['active_group'].group_id)
            else:
                user_data['active_group'] = None

            self.send_message_view(user_data)

            bot.answer_callback_query(update.callback_query.id)
            return ConversationHandler.END

        elif data == 'delete_no':
            self.send_message_view(user_data)
            bot.answer_callback_query(update.callback_query.id)
            return GroupStates.ACTION

    def back(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        bot.answer_callback_query(update.callback_query.id)

        if user_data['active_group'].group_id is not None:
            user_data['active_group'] = user_data['database_menu']['tpd'].get_group(user_data['active_group'].group_id)
            self.send_message_view(user_data)
            return GroupStates.ACTION
        else:
            user_data['active_group'] = None
            self.send_message_view(user_data)
            return ConversationHandler.END

    def next_page(self, bot, update, user_data):
        _ = user_data['_']

        if len(user_data['active_group'].groups) + len(user_data['active_group'].entries) <= ITEMS_PER_PAGE:
            bot.answer_callback_query(update.callback_query.id, text=_("There is only one page"))
        else:
            user_data['group_menu']['page'] += 1
            self.send_message_view(user_data)
            bot.answer_callback_query(update.callback_query.id)
        return GroupStates.ACTION

    def prev_page(self, bot, update, user_data):
        _ = user_data['_']
        if len(user_data['active_group'].groups) + len(user_data['active_group'].entries) <= ITEMS_PER_PAGE:
            bot.answer_callback_query(update.callback_query.id, text=_("There is only one page"))
        else:
            user_data['group_menu']['page'] -= 1
            self.send_message_view(user_data)
            bot.answer_callback_query(update.callback_query.id)
        return GroupStates.ACTION

    def search(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']
        text = update.message.text.replace('/search ', '')
        entries = user_data['database_menu']['tpd'].search(text)
        if entries:
            fake_group = TelegramPasswordGroup(id=None, title=_("Search results"), notes="")
            fake_group.fake = True
            fake_group.entries = entries
            fake_group.group_id = user_data['active_group'].id
            user_data['active_group'] = fake_group
        else:
            user_data['interface'].delete()
            user_data['interface'] = None
            bot.send_message(chat_id=user.chat_id, text=_("Nothing found."))
        self.send_message_view(user_data)
        return GroupStates.ACTION

    def import_error(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        user_data['interface'].delete()

        user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("For importing you mast be in root group."))
        user_data['interface'] = None

        self.send_message_view(user_data)

        return GroupStates.ACTION

    def get_handler(self):

        edit_menu = EditMenu()
        self.menus.append(edit_menu)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^group_\d+', pass_user_data=True)],
                                      states={
                                          GroupStates.ACTION: [edit_menu.handler,
                                                               CallbackQueryHandler(self.next_page, pattern=r'^next_page$', pass_user_data=True),
                                                               CallbackQueryHandler(self.prev_page, pattern=r'^previous_page', pass_user_data=True),
                                                               CommandHandler('search', self.search, pass_user_data=True),
                                                               CommandHandler('import', self.import_error, pass_user_data=True),
                                                               CallbackQueryHandler(self.back, pattern=r'^back$', pass_user_data=True),
                                                               CallbackQueryHandler(self.delete, pattern=r'^delete$', pass_user_data=True),
                                                               MessageHandler(filters.Filters.all, self.to_state(GroupStates.ACTION))],

                                          GroupStates.DELETE_CONFIRMATION: [CallbackQueryHandler(self.delete_confirmation, pattern=r'delete_(yes|no)$', pass_user_data=True)]
                                      },
                                      fallbacks=[MessageHandler(filters.Filters.all, self.unknown_command(-1), pass_user_data=True)],
                                      allow_reentry=True)

        return handler
