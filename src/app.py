import logging
import os
import pprint
from datetime import timedelta
from gettext import gettext

import coloredlogs
from emoji import emojize
from telegram import Bot, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import Updater, MessageHandler, Filters, CommandHandler, CallbackQueryHandler

from src.handlers.start import StartMenu
from src.lib.save_jobs import save_jobs_job, load_jobs, save_jobs
from src.models import User, DBSession
from src.settings import BOT_TOKEN, MEDIA_FOLDER, RESOURCES_FOLDER, WEBHOOK_ENABLE, WEBHOOK_URL, WEBHOOK_IP, WEBHOOK_PORT

logger = logging.getLogger(__name__)


def stop_ask(bot, update, user_data):
    _ = user_data['_'] if '_' in user_data else gettext

    reply_markup = InlineKeyboardMarkup([[InlineKeyboardButton(emojize(":white_heavy_check_mark: ") + _("Yes") + emojize(" :white_heavy_check_mark:"), callback_data='stop_yes')],
                                         [InlineKeyboardButton(emojize(":no_entry:️ ") + _("No") + emojize(" :no_entry:️"), callback_data='stop_no')]])

    bot.send_message(chat_id=update.message.chat_id, text=_("You really want to delete all data related to you from bot?"), reply_markup=reply_markup)


def stop(bot, update, user_data):
    _ = user_data['_'] if '_' in user_data else gettext
    user = DBSession.query(User).filter(User.chat_id == user_data['user'].chat_id).first()
    DBSession.delete(user)
    DBSession.commit()
    bot.send_message(chat_id=user_data['user'].chat_id, text=_("All data related to you deleted. If you want start again send me /start"))


def info(bot, update, user_data):
    _ = user_data['_'] if '_' in user_data else gettext

    message_text = _("1. To change language use \"/start\" command.") + '\n'
    message_text += _("2. You can search entries with command \"/search text\".") + '\n'
    message_text += _("3. If you want to import your kdbx database into bot you must log in your any database and then use \"/import\" command.")  + '\n'
    message_text += _("4. When using bot it's recommended to delete all your messages which contains username, email, password, key-file or .kdbx database.")  + '\n'
    message_text += _("5. All your data is stored encrypted with your personal password.") + '\n'
    message_text += _("6. Bot do not save your password in its original form.") + '\n'
    message_text += _("7. For data encrypting bot uses AES128 algorithm.") + '\n'
    message_text += _("8. You can delete all data related to you from bot with command \"/stop\".")

    buttons_messate_text = emojize(":reverse_button:") + _(" - Previous page/field") + '\n'
    buttons_messate_text += emojize(":play_button:") + _(" - Next page/field") + '\n'
    buttons_messate_text += emojize(":up_arrow:") + _(" - Go up to previous menu/group") + '\n'
    buttons_messate_text += emojize(":NEW_button:") + _(" - Create new entry/group") + '\n'
    buttons_messate_text += emojize(":memo:") + _(" - Edit database/group/entry which currently selected") + '\n'
    buttons_messate_text += emojize(":repeat_button:") + _(" - Resend \"interface\" message") + '\n'
    buttons_messate_text += emojize(":down_arrow:") + _(" - Download database as kdbx file") + '\n'
    buttons_messate_text += emojize(":cross_mark:") + _(" - Delete database/group/entry which currently selected") + '\n'
    buttons_messate_text += _("P.S. Interface above this message still working")

    bot.send_message(chat_id=update.message.chat_id, text=message_text)
    bot.send_message(chat_id=update.message.chat_id, text=buttons_messate_text)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    pp = pprint.PrettyPrinter(indent=4)
    logger.error(f'Update "{pp.pformat(str(update))}" caused error "{error}"')


def main():
    bot = Bot(token=BOT_TOKEN)
    updater = Updater(token=BOT_TOKEN)
    job_queue = updater.job_queue
    dispatcher = updater.dispatcher
    coloredlogs.install()

    try:
        load_jobs(job_queue)
    except FileNotFoundError:
        logger.error("Cant load jobs because file not found")

    if not job_queue.get_jobs_by_name("SaveJobs"):
        job_queue.run_repeating(save_jobs_job, timedelta(minutes=1), name="SaveJobs")

    # Handlers
    start_menu = StartMenu()
    start_menu.load_data(dispatcher)

    info_handler = CommandHandler('help', info, pass_user_data=True)
    stop_handler = CommandHandler('stop', stop_ask, pass_user_data=True)
    stop_commit_handler = CallbackQueryHandler(stop, pattern=r'^stop_(yes|no)$', pass_user_data=True)

    unknown_handler = MessageHandler(Filters.all, lambda bot, update: bot.send_message(chat_id=update.message.chat_id, text="Извините, неверная команда."))

    # adding menus
    dispatcher.add_handler(info_handler)
    dispatcher.add_handler(stop_handler)
    dispatcher.add_handler(stop_commit_handler)
    dispatcher.add_handler(start_menu.handler)
    dispatcher.add_handler(unknown_handler)
    dispatcher.add_error_handler(error)

    # Start bot
    if not os.path.exists(MEDIA_FOLDER):
        os.mkdir(MEDIA_FOLDER)
        logger.info("Media folder created")
    if not os.path.exists(RESOURCES_FOLDER):
        os.mkdir(RESOURCES_FOLDER)
        logger.info("Resources folder created")

    if WEBHOOK_ENABLE:
        update_queue = updater.start_webhook(listen=WEBHOOK_IP, port=WEBHOOK_PORT, allowed_updates=['message', 'edited_message', 'callback_query'])
        updater.bot.set_webhook(url=WEBHOOK_URL)
    else:
        update_queue = updater.start_polling(allowed_updates=['message', 'edited_message', 'callback_query'])

    logger.info("Bot started")
    updater.idle()

    for user in DBSession.query(User).filter(User.active == True).all():
        user_data = dispatcher.user_data[user.chat_id]
        if 'interface' in user_data:
            user_data['interface'].delete()

        if 'database_menu' in user_data and 'tpd' in user_data['database_menu']:
            del user_data['database_menu']['tpd']
        if 'active_group' in user_data:
            user_data['active_group'] = None

        if '_' in user_data:
            _ = user_data['_']
        else:
            _ = gettext
        bot.send_message(chat_id=user.chat_id, text=_("Bot is restarting, your database closed."))
        if 'user' in user_data:
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Welcome, choose your password database:"), reply_markup=start_menu.databases_markup(user_data))
        else:
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text="Please send /start to continue.")

    start_menu.save_user_data(dispatcher)
    start_menu.save_states(dispatcher)
    save_jobs(job_queue)


if __name__ == "__main__":
    main()
