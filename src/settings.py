import logging.config
from os import path

from src.lib.helpers import parse_environ_settings

PROJECT_ROOT = path.dirname(path.realpath(__file__))

DATABASE = "postgresql://password_manager:password_manager@localhost:5432/password_manager"  # override in local settings
BOT_TOKEN = ""  # override in local settings

BOT_NAME = "TelegramPasswordManager"

SECRET = "some secret phraze"
ITEMS_PER_PAGE = 3

"""---------------------------- WEBHOOK ---------------------------"""
WEBHOOK_ENABLE = False
WEBHOOK_URL = "/"
WEBHOOK_IP = '127.0.0.1'
WEBHOOK_PORT = -1

MEDIA_FOLDER = path.join(PROJECT_ROOT, '..', 'media')
RESOURCES_FOLDER = path.join(PROJECT_ROOT, '..', 'resources')
KDBX_SAMPLE_NAME = "EmptySample.kdbx"
KDBX_SAMPLE_PASSWORD = "SomeSecretPassword"


logging.config.dictConfig({
                            'version': 1,
                            'disable_existing_loggers': False,
                            'formatters': {
                                'default': {
                                    'format': '%(asctime)s-%(name)s-%(levelname)s-%(message)s'
                                },
                            },
                            'handlers': {
                                'console': {
                                    'level': 'INFO',
                                    'formatter': 'default',
                                    'class': 'logging.StreamHandler',
                                },
                                'deb_file': {
                                    'level': 'DEBUG',
                                    'formatter': 'default',
                                    'class': 'logging.handlers.RotatingFileHandler',
                                    'maxBytes': 10485760,  # 10MB
                                    'backupCount': 5,
                                    'encoding': 'utf8',
                                    'filename': path.join(PROJECT_ROOT, '..', 'logs', 'app.log')
                                },
                                'err_file': {
                                    'level': 'ERROR',
                                    'formatter': 'default',
                                    'class': 'logging.handlers.RotatingFileHandler',
                                    'maxBytes': 10485760,  # 10MB
                                    'backupCount': 5,
                                    'encoding': 'utf8',
                                    'filename': path.join(PROJECT_ROOT, '..', 'logs', 'error.log')
                                },
                            },
                            'loggers': {
                                '': {
                                    'handlers': ['console', 'deb_file', 'err_file'],
                                    'level': 'DEBUG',
                                    'propagate': True
                                },
                            }
                        })

parse_environ_settings(locals())

try:
    from src.local_settings import *
except ImportError:
    pass
