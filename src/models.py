import datetime
import enum
import os
import uuid

from sqlalchemy import Column, Integer, String, create_engine, DateTime, Boolean, Enum, ForeignKey, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, backref

from src.lib.crypt import AESCipher
from src.settings import DATABASE

Base = declarative_base()
engine = create_engine(DATABASE)


class Languages(enum.Enum):
    en = 'en'
    ru = 'ru'


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    chat_id = Column(Integer)
    name = Column(String)
    username = Column(String)
    active = Column(Boolean)
    join_date = Column(DateTime)
    lang = Column(Enum(Languages))


class PasswordDatabase(Base):
    __tablename__ = 'password_databases'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    user = relationship("User", backref=backref("password_databases", lazy='joined', cascade='all,delete'))
    name = Column(String, nullable=False)
    description = Column(Text)
    master_password = Column(String)
    creation_date = Column(DateTime, default=datetime.datetime.now)

    groups = relationship("DatabaseGroup", backref="password_database", cascade='all,delete')
    entries = relationship("DatabaseEntry", backref="password_database", cascade='all,delete')


class DatabaseGroup(Base):
    __tablename__ = 'database_group'

    id = Column(Integer, primary_key=True)
    title = Column(String, nullable=False)
    notes = Column(Text, default="")
    creation_date = Column(DateTime, default=datetime.datetime.now)
    entries = relationship("DatabaseEntry", backref="group", cascade='all,delete')
    uuid = Column(String, default=uuid.uuid4)

    database_id = Column(Integer, ForeignKey('password_databases.id'), nullable=False)

    parent_group_id = Column(Integer, ForeignKey('database_group.id'))
    groups = relationship("DatabaseGroup", backref=backref('parent_group', remote_side=[id]), cascade='all,delete')


class DatabaseEntry(Base):
    __tablename__ = 'database_entry'

    id = Column(Integer, primary_key=True)
    title = Column(String, nullable=False)
    username = Column(String, default="")
    password = Column(String, default="")
    url = Column(String, default="")
    notes = Column(Text, default="")
    creation_date = Column(DateTime, default=datetime.datetime.now)
    uuid = Column(String, default=uuid.uuid4)

    database_id = Column(Integer, ForeignKey('password_databases.id'), nullable=False)
    group_id = Column(Integer, ForeignKey('database_group.id'))


Session = sessionmaker(bind=engine)
DBSession = Session()


class TelegramPasswordEntry:
    def __init__(self, id, title, password, username=None, url=None, notes=None, group_id=None):
        self.id = id
        self.title = title
        self.username = username
        self.password = password
        self.url = url
        self.notes = notes
        self.group_id = group_id


class TelegramPasswordGroup:
    def __init__(self, id, title, notes, group_id=None):
        self.id = id
        self.title = title
        self.notes = notes
        self.groups = []
        self.entries = []
        self.group_id = group_id

    def add_group(self, group):
        assert isinstance(group, TelegramPasswordGroup)
        self.groups.append(group)

    def add_entry(self, entry):
        assert isinstance(entry, TelegramPasswordEntry)
        if entry.group_id is None:
            entry.group_id = self.id
        self.entries.append(entry)

    def get_entry(self, id):
        for entry in self.entries:
            if entry.id == id:
                return entry

        for group in self.groups:
            finded_entry = group.get_entry(id)
            if finded_entry:
                return finded_entry

    def get_group(self, id):
        finded_group = None
        for group in self.groups:
            if group.id == id:
                finded_group = group
                break
            else:
                finded_group = group.get_group(id)
                if finded_group:
                    break
        return finded_group

    def search(self, text):
        finded = []
        for group in self.groups:
            finded += group.search(text)

        for entry in self.entries:
            if text.lower() in entry.title.lower():
                finded.append(entry)

        return finded

    def delete_entry(self, entry):
        if entry in self.entries:
            self.entries.remove(entry)

    def fill_groups(self, db_group, aes_c):
        if db_group.groups:
            for group in db_group.groups:

                d_group_title = aes_c.decrypt(group.title)
                d_group_notes = aes_c.decrypt(group.notes)

                tpg = TelegramPasswordGroup(group.id, d_group_title, d_group_notes, self.id)

                for entry in group.entries:
                    d_title = aes_c.decrypt(entry.title)
                    d_password = aes_c.decrypt(entry.password)
                    d_username = aes_c.decrypt(entry.username)
                    d_url = aes_c.decrypt(entry.url)
                    d_notes = aes_c.decrypt(entry.notes)

                    tpg.add_entry(TelegramPasswordEntry(entry.id, d_title, d_password, d_username, d_url, d_notes, tpg.id))

                tpg.fill_groups(group, aes_c)
                self.groups.append(tpg)


class TelegramPasswordDatabase:
    def __init__(self, master_password, database_id):
        self.id = database_id
        self.title = ""
        self.description = ""
        self.groups = []
        self.entries = []
        self.load_data(master_password)

    def search(self, text):
        finded = []
        for group in self.groups:
            finded += group.search(text)

        for entry in self.entries:
            if text.lower() in entry.title.lower():
                finded.append(entry)

        return finded

    def get_entry(self, id):
        for entry in self.entries:
            if entry.id == id:
                return entry

        for group in self.groups:
            finded_entry = group.get_entry(id)
            if finded_entry:
                return finded_entry

    def get_group(self, id):
        finded_group = None
        for group in self.groups:
            if group.id == id:
                finded_group = group
                break
            else:
                finded_group = group.get_group(id)
                if finded_group:
                    break
        return finded_group

    def delete_group(self, group):
        parent_group = self.get_group(group.group_id)
        if parent_group is None:
            parent_group = self
        for gr in parent_group.groups:
            if group.id == gr.id:
                parent_group.groups.remove(gr)
                break

    def delete_entry(self, entry):
        parent_group = self.get_group(entry.group_id)
        if parent_group is None:
            parent_group = self
        for entr in parent_group.entries:
            if entr.id == entry.id:
                parent_group.entries.remove(entry)
                break

    def load_data(self, master_password):
        db = DBSession.query(PasswordDatabase).filter(PasswordDatabase.id == self.id).first()
        if isinstance(master_password, bytes):
            key = master_password
        else:
            key = "{: <32}".format(master_password).encode("utf-8")
        aes_c = AESCipher(key)
        self.title = db.name
        self.description = db.description

        groups = DBSession.query(DatabaseGroup).filter(DatabaseGroup.database_id == self.id).filter(DatabaseGroup.parent_group_id == None).all()

        for group in groups:
            d_group_title = aes_c.decrypt(group.title)
            d_group_notes = aes_c.decrypt(group.notes)

            tpg = TelegramPasswordGroup(group.id, d_group_title, d_group_notes, None)

            for entry in group.entries:
                d_title = aes_c.decrypt(entry.title)
                d_password = aes_c.decrypt(entry.password)
                d_username = aes_c.decrypt(entry.username)
                d_url = aes_c.decrypt(entry.url)
                d_notes = aes_c.decrypt(entry.notes)

                tpg.add_entry(TelegramPasswordEntry(entry.id, d_title, d_password, d_username, d_url, d_notes, tpg.id))
            tpg.fill_groups(group, aes_c)
            self.groups.append(tpg)

        entries = DBSession.query(DatabaseEntry).filter(DatabaseEntry.database_id == self.id).filter(DatabaseEntry.group_id == None).all()
        for entry in entries:
            d_e_title = aes_c.decrypt(entry.title)
            d_e_password = aes_c.decrypt(entry.password)
            d_e_username = aes_c.decrypt(entry.username)
            d_e_url = aes_c.decrypt(entry.url)
            d_e_notes = aes_c.decrypt(entry.notes)

            self.entries.append(TelegramPasswordEntry(entry.id, d_e_title, d_e_password, d_e_username, d_e_url, d_e_notes))
