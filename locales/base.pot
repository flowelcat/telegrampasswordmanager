# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2018-12-12 02:39+EET\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING\n"
"Generated-By: pygettext.py 1.5\n"


#: src/app.py:24 src/handlers/database.py:91 src/handlers/entry.py:66
#: src/handlers/group.py:43
msgid "Yes"
msgstr ""

#: src/app.py:25 src/handlers/database.py:92 src/handlers/entry.py:67
#: src/handlers/group.py:44
msgid "No"
msgstr ""

#: src/app.py:27
msgid "You really want to delete all data related to you from bot?"
msgstr ""

#: src/app.py:35
msgid "All data related to you deleted. If you want start again send me /start"
msgstr ""

#: src/app.py:41
msgid "1. To change language use \"/start\" command."
msgstr ""

#: src/app.py:42
msgid "2. You can search entries with command \"/search text\"."
msgstr ""

#: src/app.py:43
msgid "3. If you want to import your kdbx database into bot you must log in your any database and then use \"/import\" command."
msgstr ""

#: src/app.py:44
msgid "4. When using bot it's recommended to delete all your messages which contains username, email, password, key-file or .kdbx database."
msgstr ""

#: src/app.py:45
msgid "5. All your data is stored encrypted with your personal password."
msgstr ""

#: src/app.py:46
msgid "6. Bot do not save your password in its original form."
msgstr ""

#: src/app.py:47
msgid "7. For data encrypting bot uses AES128 algorithm."
msgstr ""

#: src/app.py:48
msgid "8. You can delete all data related to you from bot with command \"/stop\"."
msgstr ""

#: src/app.py:50
msgid " - Previous page/field"
msgstr ""

#: src/app.py:51
msgid " - Next page/field"
msgstr ""

#: src/app.py:52
msgid " - Go up to previous menu/group"
msgstr ""

#: src/app.py:53
msgid " - Create new entry/group"
msgstr ""

#: src/app.py:54
msgid " - Edit database/group/entry which currently selected"
msgstr ""

#: src/app.py:55
msgid " - Resend \"interface\" message"
msgstr ""

#: src/app.py:56
msgid " - Download database as kdbx file"
msgstr ""

#: src/app.py:57
msgid " - Delete database/group/entry which currently selected"
msgstr ""

#: src/app.py:58
msgid "P.S. Interface above this message still working"
msgstr ""

#: src/app.py:130
msgid "Bot is restarting, your database closed."
msgstr ""

#: src/app.py:132 src/handlers/start.py:92 src/handlers/start.py:142
#: src/handlers/start.py:160
msgid "Welcome, choose your password database:"
msgstr ""

#: src/handlers/add_edit.py:30
msgid "Entry"
msgstr ""

#: src/handlers/add_edit.py:30
msgid "Group"
msgstr ""

#: src/handlers/add_edit.py:31 src/handlers/base.py:119
#: src/handlers/database.py:227
msgid "Back"
msgstr ""

#: src/handlers/add_edit.py:33
msgid "What you want create?"
msgstr ""

#: src/handlers/add_edit.py:59 src/handlers/add_edit.py:72
#: src/handlers/add_edit.py:93 src/handlers/add_edit.py:103
msgid "Title"
msgstr ""

#: src/handlers/add_edit.py:60 src/handlers/add_edit.py:73
msgid "Username"
msgstr ""

#: src/handlers/add_edit.py:61 src/handlers/add_edit.py:74
msgid "Password"
msgstr ""

#: src/handlers/add_edit.py:62 src/handlers/add_edit.py:75
msgid "URL"
msgstr ""

#: src/handlers/add_edit.py:63 src/handlers/add_edit.py:76
#: src/handlers/add_edit.py:94 src/handlers/add_edit.py:104
msgid "Notes"
msgstr ""

#: src/handlers/add_edit.py:74
msgid "Generate"
msgstr ""

#: src/handlers/add_edit.py:319
msgid "Please fill title and password fields"
msgstr ""

#: src/handlers/add_edit.py:336
msgid "Please fill title field"
msgstr ""

#: src/handlers/add_edit.py:392 src/handlers/add_edit.py:441
#: src/handlers/add_edit.py:555 src/handlers/add_edit.py:599
#: src/handlers/database.py:84
msgid "Wrong password, try again"
msgstr ""

#: src/handlers/base.py:35
msgid "---Create new---"
msgstr ""

#: src/handlers/base.py:89
msgid "There is no groups or entries yet."
msgstr ""

#: src/handlers/base.py:90
msgid "_______Page {page} of {pages}_______"
msgstr ""

#: src/handlers/base.py:256 src/handlers/base.py:271
msgid "Database error, email about this error to support@botman.com.ua"
msgstr ""

#: src/handlers/base.py:284 src/handlers/base.py:287
msgid "Something went wrong, try again later."
msgstr ""

#: src/handlers/database.py:60
msgid "Enter your password for {name}"
msgstr ""

#: src/handlers/database.py:94
msgid "You really want to delete {name} database?"
msgstr ""

#: src/handlers/database.py:139 src/handlers/database.py:154
#: src/handlers/group.py:100 src/handlers/group.py:110
msgid "There is only one page"
msgstr ""

#: src/handlers/database.py:167 src/handlers/group.py:123
msgid "Search results"
msgstr ""

#: src/handlers/database.py:174 src/handlers/group.py:131
msgid "Nothing found."
msgstr ""

#: src/handlers/database.py:180
msgid "Enter new name for your database:"
msgstr ""

#: src/handlers/database.py:197
msgid "Your database name changed to {name}"
msgstr ""

#: src/handlers/database.py:206
msgid "Please send me file of your database in .kdbx format"
msgstr ""

#: src/handlers/database.py:224
msgid "Only password"
msgstr ""

#: src/handlers/database.py:225
msgid "Only key-file"
msgstr ""

#: src/handlers/database.py:226
msgid "Password and key-file"
msgstr ""

#: src/handlers/database.py:230
msgid "With which combination your kdbx database can be opened?"
msgstr ""

#: src/handlers/database.py:243 src/handlers/database.py:249
#: src/handlers/database.py:317 src/handlers/database.py:325
msgid "Please enter your password from imported database"
msgstr ""

#: src/handlers/database.py:246 src/handlers/database.py:265
#: src/handlers/database.py:321
msgid "Please send your file-key which associated with imported database"
msgstr ""

#: src/handlers/database.py:268 src/handlers/database.py:292
#: src/handlers/database.py:309
msgid "Please enter your password from current selected database:"
msgstr ""

#: src/handlers/database.py:273 src/handlers/database.py:335
#: src/handlers/database.py:481
msgid "Wrong input, try again"
msgstr ""

#: src/handlers/database.py:308
msgid "Wrong password from current selected database, try again"
msgstr ""

#: src/handlers/database.py:316
msgid "Wrong kdbx password, try again"
msgstr ""

#: src/handlers/database.py:320
msgid "Wrong kdbx keyfile, try again"
msgstr ""

#: src/handlers/database.py:324
msgid "Wrong password or keyfile, try again."
msgstr ""

#: src/handlers/database.py:361
msgid "Group with title \"{title}\" already in database."
msgstr ""

#: src/handlers/database.py:361 src/handlers/database.py:369
msgid "Unknown title"
msgstr ""

#: src/handlers/database.py:369
msgid "Entry with title \"{title}\" already in database."
msgstr ""

#: src/handlers/database.py:386
msgid ""
"When importing some messages appears:\n"
msgstr ""

#: src/handlers/database.py:435
msgid "Send me the password with which the exported file will be encrypted"
msgstr ""

#: src/handlers/database.py:474
msgid ""
"It's your exported kdbx database.\n"
"Save it and then press \"Back\" to return to menu."
msgstr ""

#: src/handlers/entry.py:69
msgid "You really want to delete {name} entry?"
msgstr ""

#: src/handlers/entry.py:127 src/handlers/group.py:141
msgid "For importing you mast be in root group."
msgstr ""

#: src/handlers/group.py:46
msgid "You really want to delete {name} group?"
msgstr ""

#: src/handlers/start.py:86
msgid "Read /help for aditional info."
msgstr ""

#: src/handlers/start.py:89
msgid "Welcome, you new here."
msgstr ""

#: src/handlers/start.py:89 src/handlers/start.py:104
msgid "Please send me your new password database name:"
msgstr ""

#: src/handlers/start.py:97 src/handlers/start.py:122
#: src/handlers/start.py:147
msgid "Wrong input."
msgstr ""

#: src/handlers/start.py:118
msgid "Input your new password for this database:"
msgstr ""

#: src/handlers/start.py:141
msgid "Your password database created."
msgstr ""

#: src/handlers/start.py:159
msgid "For importing you mast log into database first."
msgstr ""

